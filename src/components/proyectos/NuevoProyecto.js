import React, { useState, useContext } from 'react';
import proyectoContext from '../../context/proyectos/proyectoContext';

const NuevoProyecto = () => {
  const {
    formulario,
    errorformulario,
    mostrarFormulario,
    agregarProyecto,
    errorFormulario,
  } = useContext(proyectoContext);

  const stateProyectoIni = {
    nombre: '',
  };
  const [proyecto, setProyecto] = useState(stateProyectoIni);
  const { nombre } = proyecto;
  const handleOnchangeProyecto = (e) => {
    setProyecto({ ...proyecto, [e.target.name]: e.target.value });
  };
  //funcion par el submit
  const handleOnsubmitProyecto = (e) => {
    e.preventDefault();
    if (nombre === '') {
      errorFormulario();
      return;
    }
    agregarProyecto(proyecto);
    setProyecto(stateProyectoIni);
  };
  //mostrar formulario
  const onclick = () => {
    mostrarFormulario();
  };

  return (
    <>
      <button
        type="button"
        className="btn btn-block btn-primario"
        onClick={onclick}
      >
        Nuevo Proyecto
      </button>
      {formulario ? (
        <form
          className="formulario-nuevo-proyecto"
          onSubmit={handleOnsubmitProyecto}
        >
          <input
            type="text"
            className="input-text"
            name="nombre"
            placeholder="Nombre Proyecto"
            onChange={handleOnchangeProyecto}
            value={nombre}
          />
          <input
            type="submit"
            className="btn btn-block btn-primario"
            value="Agregar Proyecto"
          />
        </form>
      ) : null}
      {errorformulario ? (
        <p className="error mensaje">El nombre es Obligatorio</p>
      ) : null}
    </>
  );
};

export default NuevoProyecto;
