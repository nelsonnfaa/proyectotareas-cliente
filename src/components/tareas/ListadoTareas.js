import React, { useContext } from 'react';
import Tarea from './Tarea';
import proyectoContext from '../../context/proyectos/proyectoContext';
import tareaContext from '../../context/tareas/tareaContext';
import { CSSTransition, TransitionGroup } from 'react-transition-group';

const ListadoTareas = () => {
  //CONTEXT PARA PROYECTOS
  const proyectosContext = useContext(proyectoContext);
  const { proyecto, eliminaProyecto } = proyectosContext;
  //CONTEXT PARA TAREAS
  const tareasContext = useContext(tareaContext);
  const { tareasProyecto } = tareasContext;

  if (!proyecto) return <h2>Selecciona un Proyecto</h2>;
  const [proyectoActual] = proyecto;
  // const [tareasProyecto] = tareasProyecto;
  //const tareasProyecto = []
  return (
    <>
      <h2>Proyecto: {proyectoActual.nombre}</h2>
      <ul className="listado-tareas">
        {tareasProyecto.length === 0 ? (
          <li className="tarea sombra">
            <p>No hay tareas</p>
          </li>
        ) : (
          <TransitionGroup>
            {tareasProyecto.map((tarea) => (
              <CSSTransition key={tarea._id} timeout={300} classNames='proyecto'>
                <Tarea
                  key={tarea.id}
                  tarea={tarea}
                  proyecto={proyectoActual._id}
                />
              </CSSTransition>
            ))}
          </TransitionGroup>
        )}
      </ul>
      <button
        type="button"
        className="btn btn-eliminar"
        onClick={() => eliminaProyecto(proyectoActual._id)}
      >
        Eliminar Proyecto &times;
      </button>
    </>
  );
};

export default ListadoTareas;
