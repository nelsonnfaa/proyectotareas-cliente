import React, { useContext } from 'react';
import tareaContext from '../../context/tareas/tareaContext';

const Tarea = ({ tarea, proyecto }) => {
  //CONTEXT PARA TAREAS
  const tareasContext = useContext(tareaContext);
  const {
    eliminarTarea,
    obtenerTareas,
    actualizarTarea,
    guardarTareaActual,
  } = tareasContext;
  const handelOnclickEliminarTarea = () => {
    eliminarTarea(tarea._id, proyecto);
    //console.log(proyecto);
    obtenerTareas(proyecto);
  };

  const handleOnclickEstado = () => {
    if (tarea.estado) {
      tarea.estado = 'false';
      actualizarTarea(tarea);
    } else {
      tarea.estado = 'true';
      actualizarTarea(tarea);
    }
  };

  //seleccionar tarea a editar
  const handleOnclickSeleccionarTarea = () => {
    guardarTareaActual(tarea);
  };

  return (
    <li className="tarea sombra">
      <p>{tarea.nombre}</p>
      <div className="estado">
        {tarea.estado ? (
          <button
            type="button"
            className="completo"
            onClick={handleOnclickEstado}
          >
            Completo
          </button>
        ) : (
          <button
            type="button"
            className="incompleto"
            onClick={handleOnclickEstado}
          >
            Incompleto
          </button>
        )}
      </div>
      <div className="acciones">
        <button
          type="button"
          className="btn btn-primario"
          onClick={handleOnclickSeleccionarTarea}
        >
          Editar
        </button>
        <button
          type="button"
          className="btn btn-secundario"
          onClick={handelOnclickEliminarTarea}
        >
          Eliminar
        </button>
      </div>
    </li>
  );
};

export default Tarea;
