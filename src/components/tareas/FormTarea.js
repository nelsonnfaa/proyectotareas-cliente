import React, { useContext, useState, useEffect } from 'react';
import proyectoContext from '../../context/proyectos/proyectoContext';
import tareaContext from '../../context/tareas/tareaContext';

const FormTarea = () => {
  //context de proyecto
  const proyectosContext = useContext(proyectoContext);
  const { proyecto } = proyectosContext;

  //context de tareas
  const tareasContext = useContext(tareaContext);
  const {
    tareaseleccionada,
    agregarTarea,
    obtenerTareas,
    actualizarTarea,
  } = tareasContext;
  //constante para la iniciacion de tarea
  const tareaIni = {
    nombre: '',
  };
  //useeffect para estar pendiente de si selecciona una tarea.
  useEffect(() => {
    const seleccinarUnaTarea = () => {
      if (tareaseleccionada !== null) {
        setTarea(tareaseleccionada);
      } else {
        setTarea(tareaIni);
      }
    };
    seleccinarUnaTarea();
    //eslint-disable-next-line
  }, [tareaseleccionada]);

  const [tarea, setTarea] = useState(tareaIni);
  const [error, setError] = useState(false);
  if (!proyecto) return null;
  const handleOnsubmit = (e) => {
    e.preventDefault();
    if (tarea.nombre.trim() === '') {
      setError(true);
      return;
    }
    setError(false);

    //comprobar si se esta editanto o es nueva tarea
    if (tareaseleccionada === null) {
      //console.log(proyecto)
      tarea.proyecto = proyecto[0]._id;
      agregarTarea(tarea);
    } else {
      actualizarTarea(tarea);
    }

    //filtrar nuevamente tareas
    obtenerTareas(proyecto[0]._id);
    // reset form
    setTarea(tareaIni);
  };
  const handleChange = (e) => {
    setTarea({
      ...tarea,
      [e.target.name]: e.target.value,
    });
  };

  return (
    <div className="formulario">
      <form onSubmit={handleOnsubmit}>
        <div className="contenedor-input">
          <input
            type="text"
            className="input-text"
            placeholder="Nombre Tarea..."
            name="nombre"
            value={tarea.nombre}
            onChange={handleChange}
          />
        </div>
        <div className="contenedor-input">
          <input
            type="submit"
            className="btn btn-primario btn-submit btn-block"
            value={tareaseleccionada ? 'Editar Tarea' : 'Agregar Tarea'}
          />
        </div>
        {error ? <p class="error mensaje">El nombre es Obligatorio</p> : null}
      </form>
    </div>
  );
};

export default FormTarea;
