import React, { useState, useContext, useEffect } from 'react';
import { Link } from 'react-router-dom';
import AlertaContext from '../../context/alertas/alertaContext';
import AuthContext from '../../context/autenticacion/authContext';

const NuevaCuenta = (props) => {
  const alertaContext = useContext(AlertaContext);
  const { alerta, mostrarAlerta } = alertaContext;

  //para crear usuario
  const authContext = useContext(AuthContext);
  const { mensaje, autenticado, registrarUsuario } = authContext;

  const [datosUsuario, setDatosUsuario] = useState({
    nombre: '',
    email: '',
    password: '',
    confirmar: '',
  });

  //useeffect para controlar el login y error de nuevo usuario
  useEffect(() => {
    if (autenticado) {
      props.history.push('/proyectos');
    }
    if (mensaje) {
      mostrarAlerta(mensaje.msg, mensaje.categoria);
    }
    //eslint-disable-next-line
  }, [mensaje, autenticado, props.history]);

  const { nombre, email, password, confirmar } = datosUsuario;
  const handleOnchange = (e) => {
    setDatosUsuario({
      ...datosUsuario,
      [e.target.name]: e.target.value,
    });
  };
  const handleOnsubmit = (e) => {
    e.preventDefault();
    //validar campos
    if (
      nombre.trim() === '' ||
      email.trim() === '' ||
      password.trim() === '' ||
      confirmar.trim() === ''
    ) {
      mostrarAlerta('Todos los campos son obligatorios', 'alerta-error');
      return;
    }
    //Password minimo 6 caracteres
    if (password.length < 6) {
      mostrarAlerta(
        'El password debe ser al menos de 6 caracteres',
        'alerta-error'
      );
      return;
    }
    //que los dos password sean iguales
    if (password.toString !== confirmar.toString) {
      mostrarAlerta('los password deben ser iguales', 'alerta-error');
      return;
    }
    //pasar al action
    registrarUsuario({
      nombre,
      email,
      password,
    });
  };

  return (
    <div className="form-usuario">
      <div className="contenedor-form sombra-dark">
        <h1>Nueva Cuenta</h1>
        {alerta ? (
          <div className={`alerta ${alerta.categoria}`}>{alerta.msg}</div>
        ) : null}
        <form onSubmit={handleOnsubmit}>
          <div className="campo-form">
            <label htmlFor="nombre">Usuario</label>
            <input
              type="text"
              id="nombre"
              name="nombre"
              placeholder="Tu usuario"
              onChange={handleOnchange}
              value={nombre}
            />
          </div>
          <div className="campo-form">
            <label htmlFor="email">Email</label>
            <input
              type="email"
              id="email"
              name="email"
              placeholder="Tu Email"
              onChange={handleOnchange}
              value={email}
            />
          </div>

          <div className="campo-form">
            <label htmlFor="password">Contraseña</label>
            <input
              type="password"
              id="password"
              name="password"
              placeholder="Tu Password"
              onChange={handleOnchange}
              value={password}
            />
          </div>
          <div className="campo-form">
            <label htmlFor="confirmar">Repite Contraseña</label>
            <input
              type="password"
              id="confirmar"
              name="confirmar"
              placeholder="Repite Password"
              onChange={handleOnchange}
              value={confirmar}
            />
          </div>
          <div className="campo-form">
            <input
              type="submit"
              className="btn btn-primario btn-block"
              value="Registrarse"
            />
          </div>
        </form>
        <Link to={'/'} className="enlace-cuenta">
          Iniciar Sesión
        </Link>
      </div>
    </div>
  );
};

export default NuevaCuenta;
