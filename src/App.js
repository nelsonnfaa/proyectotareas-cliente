import React from 'react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import MainProyectos from './components/proyectos/MainProyectos';
import Login from './components/auth/Login';
import NuevaCuenta from './components/auth/NuevaCuenta';

import RutaPrivada from './components/rutas/RutaPrivada'

import ProyectoState from './context/proyectos/ProyectoState';
import TareaState from './context/tareas/tareaState';
import AlertaState from './context/alertas/alertaState';
import AuthState from './context/autenticacion/authState';

import tokenAuth from './config/tokenAuth';

//revisar si hay token
const token = localStorage.getItem('token');

if (token) {
  tokenAuth(token);
}

function App() {
  return (
    <ProyectoState>
      <TareaState>
        <AlertaState>
          <AuthState>
            <Router>
              <Switch>
                <Route exact path="/" component={Login} />
                <Route exact path="/nueva-cuenta" component={NuevaCuenta} />
                <RutaPrivada exact path="/proyectos" component={MainProyectos} />
              </Switch>
            </Router>
          </AuthState>
        </AlertaState>
      </TareaState>
    </ProyectoState>
  );
}

export default App;
