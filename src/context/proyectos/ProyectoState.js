import React, { useReducer } from 'react';
import proyectoContext from './proyectoContext';
import proyectoReducer from './proyectoReducer';

import clienteAxios from '../../config/axios';

import {
  FORMULARIO_PROYECTO,
  OBTENER_PROYECTOS,
  AGREGAR_PROYECTO,
  PROYECTO_ERROR,
  VALIDAR_FORMULARIO,
  PROYECTO_ACTUAL,
  ELIMINAR_PROYECTO,
} from '../../types';

const ProyectoState = (props) => {
  const initialState = {
    proyectos: [],
    formulario: false,
    errorformulario: false,
    proyecto: null,
    mensaje: null,
  };

  //Dispatch para ejecutar acciones
  const [state, dispatch] = useReducer(proyectoReducer, initialState);

  //funciones de CRUD

  //funcion para mostar formulario nuevo proyecto
  const mostrarFormulario = () => {
    dispatch({
      type: FORMULARIO_PROYECTO,
    });
  };

  //obtener proyectos
  const obtenerProyectos = async () => {
    try {
      const proyectosBBDD = await clienteAxios.get('/api/proyectos');
      dispatch({
        type: OBTENER_PROYECTOS,
        payload: proyectosBBDD.data,
      });
    } catch (error) {
      //console.log(error);
      const alert = {
        msg: error.response.data.msg,
        categoria: 'alerta-error',
      };
      dispatch({
        type: PROYECTO_ERROR,
        payload: alert,
      });
    }
  };

  //agregar nuevo proyecto
  const agregarProyecto = async (proyecto) => {
    //proyecto.id = uuid(); ya no hace falta el id lo pondra mongo en back
    //insertar el proyecto
    try {
      const respuesta = await clienteAxios.post('/api/proyectos', proyecto);
      //console.log(respuesta.data);
      dispatch({
        type: AGREGAR_PROYECTO,
        payload: respuesta.data,
      });
    } catch (error) {
      //console.log(error);
      const alert = {
        msg: error.response.data.msg,
        categoria: 'alerta-error',
      };
      dispatch({
        type: PROYECTO_ERROR,
        payload: alert,
      });
    }
  };

  //mostrar error en fomulario nuevo proyecto
  const errorFormulario = () => {
    dispatch({
      type: VALIDAR_FORMULARIO,
    });
  };

  //Funcion para seleccionar el proyecto del usuario
  const proyectoActual = (proyectoId) => {
    dispatch({
      type: PROYECTO_ACTUAL,
      payload: proyectoId,
    });
  };

  //FUCNION PARA ELIMIAR PROYECTOS
  const eliminaProyecto = async (proyectoId) => {
    try {
      await clienteAxios.delete(`/api/proyectos/${proyectoId}`);

      dispatch({
        type: ELIMINAR_PROYECTO,
        payload: proyectoId,
      });
    } catch (error) {
      //console.log(error.response.data);
      const alert = {
        msg: error.response.data.msg,
        categoria: 'alerta-error',
      };
      dispatch({
        type: PROYECTO_ERROR,
        payload: alert,
      });
    }
  };

  return (
    <proyectoContext.Provider
      value={{
        proyectos: state.proyectos,
        formulario: state.formulario,
        errorformulario: state.errorformulario,
        proyecto: state.proyecto,
        mensaje: state.mensaje,
        mostrarFormulario,
        obtenerProyectos,
        agregarProyecto,
        errorFormulario,
        proyectoActual,
        eliminaProyecto,
      }}
    >
      {props.children}
    </proyectoContext.Provider>
  );
};

export default ProyectoState;
