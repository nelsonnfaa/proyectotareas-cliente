import React, { useReducer } from 'react';
import TareaReducer from './tareaReducer';
import TareaContext from './tareaContext';

//axios
import clienteAxios from '../../config/axios';

import {
  TAREAS_PROYECTO,
  AGREGAR_TAREA,
  ELIMINAR_TAREA,
  TAREA_ACTUAL,
  ACTUALIZAR_TAREA,
} from '../../types';

const TareaState = (props) => {
  const initialState = {
    tareasProyecto: [],
    tareaseleccionada: null,
  };
  const [state, dispatch] = useReducer(TareaReducer, initialState);

  const obtenerTareas = async (proyecto) => {
    //console.log(proyecto);
    try {
      const resultado = await clienteAxios.get('/api/tareas', {
        params: { proyecto },
      });
      //console.log(resultado.data);
      dispatch({
        type: TAREAS_PROYECTO,
        payload: resultado.data,
      });
    } catch (error) {
      console.log('obtenerTareas: ', error.response);
    }
  };
  const agregarTarea = async (tarea) => {
    //console.log(tarea);
    try {
      const resultado = await clienteAxios.post('/api/tareas', tarea);
      //tarea.id = uuid();
      //console.log(resultado);
      dispatch({
        type: AGREGAR_TAREA,
        payload: resultado.data,
      });
    } catch (error) {
      console.log('agregarTarea: ', error.response);
    }
  };

  //ELIMINAR UNA TAREA
  const eliminarTarea = async (tareaId, proyecto) => {
    try {
      await clienteAxios.delete(`api/tareas/${tareaId}`, {
        params: { proyecto },
      });
      dispatch({
        type: ELIMINAR_TAREA,
        payload: tareaId,
      });
    } catch (error) {
      console.log('eliminarTarea: ', error.response);
    }
  };

  // extraer una tarea para edición

  const guardarTareaActual = (tarea) => {
    dispatch({
      type: TAREA_ACTUAL,
      payload: tarea,
    });
  };

  //actualizar tarea
  const actualizarTarea = async (tarea) => {
    try {
      const resultado = await clienteAxios.put(`/api/tareas/${tarea._id}`, {
        params: { tarea },
      });
      dispatch({
        type: ACTUALIZAR_TAREA,
        payload: resultado.data.tareaactu,
      });
    } catch (error) {
      console.log('actualizarTarea: ', error.response);
    }
  };

  return (
    <TareaContext.Provider
      value={{
        tareas: state.tareas,
        tareasProyecto: state.tareasProyecto,
        tareaseleccionada: state.tareaseleccionada,
        obtenerTareas,
        agregarTarea,
        eliminarTarea,
        guardarTareaActual,
        actualizarTarea,
      }}
    >
      {props.children}
    </TareaContext.Provider>
  );
};

export default TareaState;
